import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { ListGroup, ListGroupItem } from 'reactstrap';

import style from './ListItems.module.scss'  

export function GroupList({children, ...props}) {
    return (
        <ListGroup {...props} flush >
            {children}
        </ListGroup>
    )
}
export function GroupListItem({active, effect=true, children, to}) {

    let location = useLocation();

    if (location.pathname===to) {
        active = true;
    }   

    return (
            <ListGroupItem className={ `${style.listGroupItem} ${active && style.active}` }>
                <Link className={`${style.link} ${effect && style.link_effect} ${active && style.active}`} to={to}>{children}</Link>
            </ListGroupItem>
    )
}