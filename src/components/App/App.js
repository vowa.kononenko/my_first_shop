import Header from '../Header/Header';
import MainPage from '../pages/MainPage/MainPage';
import Footer from '../Footer/Footer';
import Breadcrumbs from '../Breadcrumbs/Breadcrumbs';
import { Route, Switch } from 'react-router';
import CategoryPage from '../pages/CategoryPage/CategoryPage';
import InfoPage from '../pages/InfoPage/InfoPage';
import WishPage from '../pages/WishPage/WishPage';
import SideMenu from '../SideMenu/SideMenu';
import ProductPage from '../pages/ProductPage/ProductPage';
import Cart from '../pages/Card/Cart';


function App() {
  return (
    <>
      <Header/>
      {/* <SideMenu/> */}
      <Breadcrumbs/>
      <Switch>
        <Route path='/' exact component={MainPage}/>
        <Route path='/info'  component={InfoPage}/>
        <Route path='/wish'  component={WishPage}/>
        <Route path='/product'  component={ProductPage}/>
        <Route path='/cart'  component={Cart}/>
        <Route path  component={CategoryPage}/>


      </Switch>
      <Footer/>
    </>
  );
}

export default App;
