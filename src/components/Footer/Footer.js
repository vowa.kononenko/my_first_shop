import React from 'react';
import { Link } from 'react-router-dom';
import { Col, Container, Row } from 'reactstrap';


import style from './Footer.module.scss'

import srcPhone from '../../assets/svg/header/phone.svg';
import srcTelegram from '../../assets/svg/social/telegram.svg';
import srcInstagram from '../../assets/svg/social/instagram.svg';
import srcFacebook from '../../assets/svg/social/facebook.svg';


export default function Footer() {
    return (
        <footer>
            <Container fluid={true} className={style.footer__container}>
                <nav>
                    <Row>
                        <Col md={{size:2,offset:1}} sm={{size:3,offset:1}}>
                            <h4>Test shop</h4>
                            <ul className='list-unstyled'>
                                <li>
                                    <Link to='/info/about'>О нас</Link>
                                </li>
                                <li>
                                    <Link to='/info/partners'>Партнеры</Link>
                                </li>
                                <li>
                                    <Link to='/info/dealer'>Оптовым покупателям</Link>
                                </li>
                                <li>
                                    <Link to='/info/vacancies'>Вакансии</Link>
                                </li>
                            </ul>
                        </Col>
                        <Col md={{size:3, offset:0}} sm={{size:4}}>
                            <h4>Помощь</h4>
                            <ul className='list-unstyled'>
                                <li>
                                    <Link to='/info/delivery'>Доставка и оплата</Link>
                                </li>
                                <li>
                                    <Link to='/info/returns'>Обмен и возврат</Link>
                                </li>
                                <li>
                                    <Link to='/info/warranty'>Сервис и гарантия</Link>
                                </li>
                                <li>
                                    <Link to='/info/terms'>Условия использования сайта</Link>
                                </li>
                            </ul>
                        </Col>
                        <Col md={{size:3, offset:0}} sm={{size:4}}>
                            <h4>Контакты</h4>
                            <ul className='list-unstyled'>
                                <li>
                                    <span className={style.svgPhone}>
                                        <svg >
                                            <use href={srcPhone +'#Capa_1'}></use>
                                        </svg>
                                    </span>
                                    <span> C 10:00 до 18:00</span>
                                </li>
                                <li>
                                    <a className={style.footer__phoneNumber} href='tel:0962938604' content="+380962938604">(096) 293-86-04</a>
                                </li>

                            </ul>
                        </Col>
                        <Col md={{size:3, offset:0}} sm={{size:4, offset:1}}>
                            <h4>Социальные сети</h4>
                            <ul className='list-unstyled'>
                                <li>
                                    <div className={style.footer__social}>
                                            <a  target="_blank" href="https://t.me/valankka" rel="noreferrer">
                                                <svg>
                                                    <use href={srcTelegram + '#telegram'}>
                                                    </use>
                                                </svg>

                                            </a>
                                            <a target="_blank" href="https://www.instagram.com/vovane_lo" rel="noreferrer">
                                                <svg>
                                                    <use href={srcInstagram + '#instagram'}>
                                                    </use>
                                                </svg>

                                            </a>
                                            <a target="_blank" href="https://www.facebook.com/vowa.kononenko" rel="noreferrer">
                                                <svg>
                                                    <use href={srcFacebook + '#facebook'}>
                                                    </use>
                                                </svg>

                                            </a>

                                    </div>
                                    
                                </li>
                            </ul>
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={{offset:7}} md={{offset:5}} xs={{offset:1}}>
                            <p className={style.footer__copyright}>©Training store "Test shop", created Vladimir Kononenko. 2021</p>
                        </Col>
                    </Row>
                </nav>
                
            </Container>
        </footer>
        

    )
}