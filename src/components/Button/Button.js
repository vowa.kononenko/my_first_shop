import React from 'react';

import style from './Button.module.scss'

export default function Button({children, color}) {
    return(
        <button className={`${style.button} ${color === 'secondary'&& style.button_secondary}`}>
            {children}
        </button>
    )
};
