import React from 'react';
import { Link } from 'react-router-dom';
import { Card, CardBody, CardImg} from 'reactstrap';

import style from './ProductCard.module.scss'
import srcShopcart from '../../assets/svg/header/shopсart.svg';
import srcHeart from '../../assets/svg/header/heart.svg';

export default function ProductCard({title}) {
    return (
        
          <Card className={style.card}>
            <div className={style.heart}>
              <svg >
                <use href={srcHeart +'#heart'}></use>
              </svg>
            </div>
            <Link  >
              <CardImg top width="100%" src="https://i.can.ua/images/200x190/goods/4894/4894655.jpg" alt="Card image cap" />
            </Link>
            <CardBody className='p-2'>
              <Link className={style.title} >{title}</Link> 
              <div className={style.bottonBlock}>
              <p className={style.prise}>85 999 ₴</p>
                <div className={style.shopсart}>
                  <svg>
                    <use href={srcShopcart +'#Layer_1'}></use>
                  </svg>
                </div>
              </div>
            </CardBody>
          </Card>
        
    )
}