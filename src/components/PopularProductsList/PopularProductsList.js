import React from 'react';
import ProductCard from '../ProductCard/ProductCard';

import style from './PopularProductsList.module.scss'

export default function PopularProductsList() {
    return(
        <>
        <div>
            <h3 className='mt-2'>Популярные товары</h3>
        </div>

        <div>
            <ul className={style.cardGroup}>
                <li>
                <ProductCard title='Ноутбук ASUS X515JP-BQ031 (90NB0SS1-M00620)' />
                </li>
                <li>
                <ProductCard title='Ноутбук ' />
                </li>
                <li>
                <ProductCard title='Ноутбук ASUS X515JP-BQ031 (90NB0SS1-M00620)' />
                </li>                    <li>
                <ProductCard title='Ноутбук ASUS X515JP-BQ031 (90NB0SS1-M00620)' />
                </li>                    <li>
                <ProductCard title='Ноутбук ASUS X515JP-BQ031 (90NB0SS1-M00620)' />
                </li>                    <li>
                <ProductCard title='Ноутбук ASUS X515JP-BQ031 (90NB0SS1-M00620)' />
                </li>                    <li>
                <ProductCard title='Ноутбук ASUS X515JP-BQ031 (90NB0SS1-M00620)' />
                </li>                    <li>
                <ProductCard title='Ноутбук ASUS X515JP-BQ031 (90NB0SS1-M00620)' />
                </li>                    <li>
                <ProductCard title='Ноутбук ASUS X515JP-BQ031 (90NB0SS1-M00620)' />
                </li>                    <li>
                <ProductCard title='Ноутбук ASUS X515JP-BQ031 (90NB0SS1-M00620)' />
                </li>
            </ul>
        </div>
            
        </>
    )
}