import React from 'react';
import { GroupList, GroupListItem } from '../ListItems/ListItems';




export default function CategoryList({...props}) {
    return (
        <GroupList {...props} >
            <GroupListItem to='/Категория'>Категория</GroupListItem>
            <GroupListItem to='/Категория #2'>Категория #2</GroupListItem>
            <GroupListItem to='/Категория #3'>Категория #3</GroupListItem>
            <GroupListItem to='/Категория #4'>Категория #4</GroupListItem>
            <GroupListItem to='/Категория #5'>Категория #5</GroupListItem>
            <GroupListItem to='/Категория #6'>Категория #6</GroupListItem>
            <GroupListItem to='/Категория #7'>Категория #7</GroupListItem>
            <GroupListItem to='/Категория #8'>Категория #8</GroupListItem>
            <GroupListItem to='/Категория #9'>Категория #9</GroupListItem>
        </GroupList>
    )
}