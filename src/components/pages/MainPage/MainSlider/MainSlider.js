import React from "react";
import Slider from "react-slick";

import './slick.scss'
import style from './MainSlider.module.scss'

export default function MainSlider() {
    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,

    };
    return (
        <Slider {...settings} className={style.slider}>
            <div>
                <img  src="https://compx.com.ua/content/uploads/images/peripheral-discount_2888x990_ru%281%29.jpg" alt=""/>
            </div>
            <div>
                <img  src="http://img.telemart.ua/uploaded/news/2019_06_10/akcia.png" alt=""/>
            </div>
            <div>
                <img  src="https://compx.com.ua/content/uploads/images/deepcool960x432rus-1.jpg" alt=""/>
            </div>
            <div>
                <img  src="https://www.asusmarket.ru/img/1576253331_Xbox-dec19-top_.jpg" alt=""/>                
            </div>
            <div>
                <img  src="http://img.telemart.ua/uploaded/news/2020_04_23/akcia_2020.png" alt=""/>
            </div>
        </Slider>
    );
}
