import React from 'react';
import CategoryList from '../../CategoryList/CategoryList'
import {Container, Row, Col} from 'reactstrap';
import MainSlider from './MainSlider/MainSlider';

import style from './MainPage.module.scss'
import PopularProductsList from '../../PopularProductsList/PopularProductsList';

export default function MainPage() {
    return (
        <Container fluid= {true}>
            <Row>   
                <Col  md={{size: 3}} >
                    <CategoryList className={style.categoryList} />
                </Col>
                <Col md={{size: 9}} xs={{size: 12}}>
                    <Row>
                        <Col  md={{size: 10, offset:1}}   xs={{size: 10, offset:1}}>
                        <MainSlider/>
                        </Col>
                    </Row>

                </Col>
            </Row>
            <Row>
                        <Col>
                            <PopularProductsList/>
                        </Col>
                    </Row>
    </Container>
)   
            
}