import React from "react";
import { Row } from "reactstrap";

export default function Partners() {
    return (
        <div className="pageContent">
            <Row>
                <h3>Партнеры</h3>
            </Row>
            <Row>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Illum tempora veritatis accusamus reiciendis? Quidem,
                    architecto. Eum omnis asperiores laborum magni nostrum. Quia
                    rem itaque, doloremque necessitatibus soluta repellendus
                    nesciunt velit? Debitis praesentium nobis vero, repellat
                    ullam itaque atque suscipit voluptates, iure quisquam at
                    minima magnam cumque molestias, obcaecati eaque temporibus
                    nisi! Veniam facere rem facilis laboriosam? Ea consequatur
                    aspernatur ab?
                </p>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Tenetur voluptas praesentium, magni unde harum sapiente
                    laboriosam maiores neque laborum laudantium esse officiis
                    quisquam. Mollitia a fugit est! Hic, recusandae porro.
                    Consequuntur aut quae id obcaecati saepe ullam architecto,
                    quam provident iusto recusandae tempore magnam! Totam
                    provident quos, tempore ex dolor harum sed repudiandae! Illo
                    velit quos similique et esse rem!
                </p>
            </Row>
        </div>
    );
}
