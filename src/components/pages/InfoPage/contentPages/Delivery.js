import React from "react";
import { Row } from "reactstrap";

export default function Delivery() {
    return (
        <div className="pageContent">
            <Row>
                <h3>Доставка и оплата</h3>
            </Row>
            <Row>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Quae incidunt expedita amet animi officia saepe illo quasi
                    ab placeat nesciunt officiis iste eaque delectus aliquam
                    excepturi, est, quisquam, et repellendus? Voluptas autem
                    eius quo dolor, eveniet distinctio possimus sunt labore
                    error id corrupti quisquam similique animi debitis eligendi
                    quia consectetur nihil quas voluptates nemo. Optio magnam
                    error laborum nesciunt laboriosam! Dignissimos aliquam eum
                    perferendis, vero iure quia incidunt voluptatum doloribus
                    harum ut nemo molestias voluptatibus ipsum, sit fuga porro
                    est repellendus necessitatibus corporis non rem minus
                    delectus? Enim, alias quibusdam.
                </p>
            </Row>
        </div>
    );
}
