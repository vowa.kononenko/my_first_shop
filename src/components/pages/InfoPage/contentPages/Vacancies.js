import React from "react";
import { Row } from "reactstrap";

export default function Vacancies() {
    return (
        <div className="pageContent">
            <Row>
                <h3>Вакансии</h3>
            </Row>
            <Row>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Id
                    harum, rem delectus quae illo quas itaque qui explicabo
                    dolorum placeat reiciendis accusantium! Numquam corrupti
                    quaerat, vero beatae eius aliquam recusandae!
                </p>
                <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Fugit modi nihil harum sunt reiciendis dignissimos, odio
                    veritatis vero tempore mollitia nemo illo, quidem doloremque
                    minus explicabo! Fugiat, illum eos. Est. Sapiente
                    exercitationem recusandae, autem natus nobis assumenda.
                    Animi cumque quos, tempore aut deleniti nisi numquam
                    quaerat, eum odio molestiae tempora. Hic dicta a voluptas
                    praesentium nisi, quaerat at fuga ratione! Cumque, iure
                    error? Doloribus, similique ullam repudiandae, debitis dolor
                    labore magnam maxime iure et voluptas incidunt rerum quae
                    fugit obcaecati aliquid consectetur. Tenetur voluptates
                    dolores dolor laborum delectus magnam quisquam? Iste
                    eligendi eveniet necessitatibus in officiis id. Autem
                    architecto optio ipsam asperiores rerum perspiciatis
                    possimus itaque esse unde, eos libero quisquam officia
                    corrupti earum ea inventore culpa magni eaque praesentium!
                    Placeat itaque asperiores eligendi minus ipsa delectus modi
                    repellat debitis veritatis commodi! Quia iusto totam
                    voluptate explicabo exercitationem consequatur nulla facilis
                    delectus libero minus, voluptatem laboriosam itaque culpa
                    tempore reiciendis! Eveniet id, consequuntur error
                    reiciendis maiores esse tenetur accusantium fuga, rem, ab
                    laboriosam ducimus doloremque neque ipsa blanditiis
                    repellendus corrupti minus? Voluptas nobis soluta esse
                    sapiente quae laborum deleniti consequuntur!
                </p>
            </Row>
        </div>
    );
}
