import React from "react";
import { Row } from "reactstrap";

export default function About() {
    return (
        <div className="pageContent">
            <Row>
                <h3>О нас</h3>
            </Row>
            <Row>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Velit omnis deleniti ipsa perspiciatis qui exercitationem
                    corporis! Quibusdam quisquam, dicta illum iusto aliquid,
                    deleniti dolore corporis numquam quos odit qui repellendus.
                </p>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Vitae, necessitatibus modi. Sapiente nisi voluptas
                    voluptates quia aliquam, eaque quaerat ipsa incidunt
                    excepturi voluptatum repellat assumenda consectetur et
                    veritatis laudantium harum!
                </p>
            </Row>
        </div>
    );
}
