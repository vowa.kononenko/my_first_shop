import React from "react";
import { Row } from "reactstrap";

export default function Terms() {
    return (
        <div className="pageContent">
            <Row>
                <h3>Условия использования сайта</h3>
            </Row>
            <Row>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Expedita consequuntur possimus, laudantium eveniet deleniti
                    labore eligendi quia temporibus architecto numquam alias
                    sunt sapiente veniam corporis, suscipit, asperiores
                    reprehenderit ipsam explicabo. Ratione harum repudiandae
                    dolores soluta laboriosam dolor beatae qui ex, explicabo
                    porro repellendus consequatur exercitationem cumque
                    blanditiis nostrum, pariatur quos ipsum possimus iure fuga
                    dolorem molestias eligendi illo perferendis. Sunt.
                </p>
                <p>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                    Animi, neque nulla praesentium inventore nobis, quibusdam,
                    suscipit ea aut magni sequi sapiente reiciendis? Laudantium
                    perspiciatis quia cum, aliquam incidunt suscipit accusamus.
                    Enim sint blanditiis cumque, rem animi quisquam praesentium
                    qui voluptatibus consequatur, est eaque odit deleniti
                    dignissimos quam, quia aliquam minus esse molestiae placeat.
                    Quam molestiae tenetur recusandae cum id doloremque. Quod
                    ipsam voluptas beatae magni saepe culpa fugiat amet, numquam
                    autem, eum voluptatibus ab ipsum incidunt id iste porro!
                    Dicta, sunt nam? Veritatis at ducimus vero praesentium omnis
                    molestiae cupiditate! Optio temporibus excepturi voluptates,
                    eveniet deleniti voluptatibus cupiditate non explicabo
                    consectetur perspiciatis! Quae dolore aliquam sapiente
                    voluptate corrupti fugiat molestias, distinctio deserunt
                    neque ipsa ad placeat ut necessitatibus fuga expedita.
                </p>
            </Row>
        </div>
    );
}
