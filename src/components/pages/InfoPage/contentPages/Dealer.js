import React from "react";
import { Row } from "reactstrap";

export default function Dealer() {
    return (
        <div className="pageContent">
            <Row>
                <h3>Оптовым покупателям</h3>
            </Row>
            <Row>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Molestiae excepturi quaerat ab necessitatibus perspiciatis
                    ipsa praesentium cum adipisci deserunt quam, tenetur maxime
                    ut. Quibusdam, impedit voluptate! Incidunt soluta quam
                    dignissimos?
                </p>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Facilis sint, assumenda magni molestias quidem, aspernatur
                    eos expedita, libero repellat eius officia dolor sed tenetur
                    quo odit possimus aliquam ad officiis? Provident quas eaque,
                    maiores impedit ducimus eligendi odit facere reiciendis,
                    quasi at praesentium et sit. Debitis qui natus sapiente
                    laudantium impedit, veniam eligendi quisquam ab architecto,
                    iure esse, ex ducimus? Explicabo voluptas minima, ratione
                    ullam laudantium suscipit cupiditate assumenda nobis
                    nesciunt beatae quia sequi doloribus. Obcaecati repudiandae
                    eveniet quos fuga, corrupti, consequatur iure pariatur, quae
                    quasi sunt ullam cupiditate! Cum. Reiciendis quas autem
                    veritatis in voluptas ut neque quidem incidunt earum aut
                    reprehenderit eum, aliquam odio tempora sint facere alias
                    commodi ratione! Vel laboriosam facere repellendus expedita,
                    temporibus assumenda sint? Culpa pariatur, dolor nemo nulla
                    maxime autem exercitationem reprehenderit repellendus.
                    Porro, reprehenderit excepturi dignissimos dolorem ea enim
                    libero. Optio suscipit iste cum accusantium nisi ratione
                    amet quaerat laudantium eaque sunt.
                </p>
            </Row>
        </div>
    );
}
