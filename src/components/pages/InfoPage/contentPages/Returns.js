import React from "react";
import { Row } from "reactstrap";

export default function Returns() {
    return (
        <div className="pageContent">
            <Row>
                <h3>Обмен и возврат</h3>
            </Row>
            <Row>
                <p>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                    Quaerat ab harum pariatur? Non, eum rerum adipisci eos
                    repudiandae laborum placeat iure assumenda fugit iusto,
                    autem amet expedita recusandae blanditiis. Reiciendis!
                </p>
                <p>
                    Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                    Ratione odio asperiores aliquid eius animi officia officiis
                    placeat saepe consequuntur dolores! Consectetur distinctio
                    architecto autem repellendus! Placeat architecto sint earum
                    porro? Repudiandae non id modi aliquam quae et iusto quia?
                    Sint tempore explicabo aperiam sed temporibus blanditiis
                    aliquam sunt fugit molestiae voluptatibus, aliquid tenetur?
                    Alias blanditiis repudiandae ipsam explicabo assumenda
                    asperiores. Porro vitae quasi nulla id totam consequuntur
                    explicabo libero repudiandae amet aut architecto deleniti
                    corporis numquam aperiam quod, veritatis nobis dicta eveniet
                    praesentium, tempore nisi qui? Quibusdam praesentium
                    repudiandae non? Officia iusto cupiditate officiis
                    aspernatur laudantium inventore, nulla recusandae hic? Fugit
                    voluptatem a accusamus illo rerum laborum accusantium ab est
                    ipsam! Saepe nemo dolor officiis ipsum libero excepturi
                    repellat voluptatem!
                </p>
            </Row>
        </div>
    );
}
