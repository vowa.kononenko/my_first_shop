import React from "react";
import { Row } from "reactstrap";

export default function Warranty() {
    return (
        <div className="pageContent">
            <Row>
                <h3>Сервис и гарантия</h3>
            </Row>
            <Row>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Ducimus corrupti quis eius mollitia magnam, accusantium
                    similique est quae quaerat expedita, recusandae facilis
                    ratione nesciunt natus! Dignissimos cupiditate voluptate
                    fugiat sed!
                </p>
                <h5>Lorem ipsum dolor sit amet.</h5>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Enim a nulla officia, distinctio natus accusantium molestiae
                    maxime, quam est architecto, vel quas alias quasi. Maxime
                    unde laboriosam iste nobis cumque? Minus, similique nobis
                    dignissimos perspiciatis id recusandae corrupti natus
                    veritatis unde necessitatibus magnam ipsam dolore accusamus
                    quae maiores ipsa adipisci voluptas aperiam doloremque
                    temporibus excepturi. Suscipit, quia iste. Minus, qui?
                    Necessitatibus, corrupti id. Aliquid vel sed, officia iusto
                    quas error sequi, numquam eveniet incidunt itaque repellat
                    ducimus est, odio aperiam dignissimos autem possimus tenetur
                    libero assumenda quos et doloremque reiciendis.
                </p>
            </Row>
        </div>
    );
}
