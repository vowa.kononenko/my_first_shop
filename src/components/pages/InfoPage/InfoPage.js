import React from 'react';
import {Container, Row, Col} from 'reactstrap';


import InfoList from '../../InfoList/InfoList';
import About from './contentPages/About';
import { Route, Switch } from 'react-router';
import Partners from './contentPages/Partners';
import Dealer from './contentPages/Dealer';
import Vacancies from './contentPages/Vacancies';
import Delivery from './contentPages/Delivery';
import Returns from './contentPages/Returns';
import Warranty from './contentPages/Warranty';
import Terms from './contentPages/Terms';

import style from './InfoPage.module.scss'

export default function InfoPage() {
    return (
        <Container fluid= {true}>
            <Row>   
                <Col  md={{size: 3}}>
                    <InfoList className={style.infoList}/>
                </Col>
                <Col md={{size: 9}} xs={{size: 12}}>
                    <Row>
                        <Col  md={{size: 10, offset:1}}   xs={{size: 10, offset:1}}>
                            <Switch>
                                <Route path='/info/about' component={About}/>
                                <Route path='/info/partners' component={Partners}/>
                                <Route path='/info/dealer' component={Dealer}/>
                                <Route path='/info/vacancies' component={Vacancies}/>
                                <Route path='/info/delivery' component={Delivery}/>
                                <Route path='/info/returns' component={Returns}/>
                                <Route path='/info/warranty' component={Warranty}/>
                                <Route path='/info/terms' component={Terms}/>

                            </Switch>
                        </Col>
                    </Row>

                </Col>
            </Row>
    </Container>
)   
            
}