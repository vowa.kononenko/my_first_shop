import React from "react";
import CartBlock from "../CartBlock/CartBlock";
import HorizontalCard from "../HorizontalCard/HorizontalCard";

export default function CartList() {
    return(
        <CartBlock>
            <h2 className='text-center'>Товары в корзине</h2>
            <HorizontalCard/>
            <HorizontalCard/>
            <HorizontalCard/>
        </CartBlock>
    )
};
