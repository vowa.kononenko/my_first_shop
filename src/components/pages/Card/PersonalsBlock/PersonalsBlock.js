import React from "react";
import CartBlock from "../CartBlock/CartBlock";

import style from './PersonalsBlock.module.scss'

export default function PersonalsBlock() {
    return(
        <CartBlock>
            <h3 className='text-center'>Данные покупателя</h3>
            <label for='personalFirstName'>Имя покупателя:</label>
            <input type="text" className={`form-control ${style.personalsBlock__input}`} name='personalFirstName' id='personalFirstName' placeholder='Имя покупателя'/>
            <label for='personalSecondName'>Фамилия покупателя:</label>
            <input type="text" className={`form-control ${style.personalsBlock__input}`} name='personalSecondName' id='personalSecondName' placeholder='Фамилия покупателя'/>
            <label for='personalPatronymic'>Отчество покупателя:</label>
            <input type="text" className={`form-control ${style.personalsBlock__input}`} name='personalPatronymic' id='personalPatronymic' placeholder='Отчество покупателя'/>
            <label for='personalTel'>Телефон:</label>
            <input type="tel" className={`form-control ${style.personalsBlock__input}`} name='personalTel' id='personalTel' placeholder='+380'/>
            <label for='personalTel'>E-mail:</label>
            <input type="email" className={`form-control ${style.personalsBlock__input}`} name='personalEmail' id='personalTel' placeholder='example@example.com'/>
        </CartBlock>
    )
};
