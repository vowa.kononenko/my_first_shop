import React from "react";

import style from './CartBlock.module.scss'

export default function CartBlock({children}) {
    return(
        <div className={style.cartBlock}>
            {children}
        </div>
    )
};
