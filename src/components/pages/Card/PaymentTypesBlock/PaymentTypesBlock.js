import React from "react";
import CartBlock from "../CartBlock/CartBlock";

import style from './PaymentTypesBlock.module.scss'
import srcCash from '../../../../assets/svg/cash.svg';
import srcCard from '../../../../assets/svg/card.svg';
import srcReceipt from '../../../../assets/svg/receipt.svg';

export default function PaymentTypesBlock() {
    return(
        <CartBlock>
            <h3 className='text-center'>Способ оплаты</h3>
            <ul className={style.paymentTypesBlock__list}>
                <li className={style.paymentTypesBlock__listItem}>
                <input
                    type="radio"
                    name="paymentTypes"
                    id="cashChangeable"
                    value="cashChangeable"
                    className={style.paymentTypesBlock__radio}
                />
                <label className={style.paymentTypesBlock__label} for="cashChangeable">
                    <svg className={style.paymentTypesBlock__icon} >
                        <use href={srcCash +'#cash'}></use>
                    </svg>
                    <span>Наличные или картой при получении</span>
                </label>
                </li>
                <li className={style.paymentTypesBlock__listItem}>
                <input
                    type="radio"
                    name="paymentTypes"
                    id="paymentСard"
                    value="paymentСard"
                    className={style.paymentTypesBlock__radio}
                />
                <label className={style.paymentTypesBlock__label} for="paymentСard">
                    <svg className={style.paymentTypesBlock__icon} >
                        <use href={srcCard +'#card'}></use>
                    </svg>
                    <span>Visa/Mastercard</span>
                </label>
                </li>
                <li className={style.paymentTypesBlock__listItem}>
                <input
                    type="radio"
                    name="paymentTypes"
                    id="cashlessPayments"
                    value="cashlessPayments"
                    className={style.paymentTypesBlock__radio}
                />
                <label className={style.paymentTypesBlock__label} for="cashlessPayments">
                    <svg className={style.paymentTypesBlock__icon} >
                        <use href={srcReceipt +'#receipt'}></use>
                    </svg>
                    <span>Безналичный расчет</span>
                </label>
                </li>
            </ul>
        </CartBlock>
    )
};
