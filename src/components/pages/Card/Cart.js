import React from "react";
import { Col, Container, Row } from "reactstrap";
import CartList from "./CartList/CartList";
import PaymentTypesBlock from "./PaymentTypesBlock/PaymentTypesBlock";
import PersonalsBlock from "./PersonalsBlock/PersonalsBlock";
import TotalsBlock from "./TotalsBlock/TotalsBlock";



export default function Cart() {
    return(
        <Container fluid>
            <Row className='mb-4'>
                <Col xs={{size:12, offset:0}} sm={{size:10, offset:1}} xl={{size:8, offset:2}}>
                <CartList/>
                </Col>
            </Row >
            <Row >
                <Col className='mb-4' xs={{size:12, offset:0}} sm={{size:10, offset:1}} lg={{size:5, offset:1}} xl={{size:4, offset:2}}>
                    <PersonalsBlock/>
                </Col>
                <Col xs={{size:12, offset:0}} sm={{size:10, offset:1}} lg={{size:5, offset:0}} xl={{size:4, offset:0}}>
                    
                    <PaymentTypesBlock/>
                    <TotalsBlock/>

                </Col>
            </Row>

        </Container>
    )
};
