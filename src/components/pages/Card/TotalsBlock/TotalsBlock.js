import React from "react";
import CartBlock from "../CartBlock/CartBlock";
import Button from "../../../Button/Button";


import style from './TotalsBlock.module.scss'

export default function TotalsBlock() {
    return(
        <div className="mt-3">
            <CartBlock>
                <div className={style.totalsBlock__item}>
                    <span className={style.totalsBlock__title}>Доставка:</span>
                    <span className={style.totalsBlock__value}>бесплатно</span>
                </div>
                <div className={style.totalsBlock__item}>
                    <span className={style.totalsBlock__title}>Сумма заказа:</span>
                    <span className={style.totalsBlock__value}>205 599 грн</span>
                </div>
            </CartBlock>
            <div className={style.totalsBlock__submit}>
                <Button color='secondary' >Оформить заказ</Button>

            </div>
        </div>

    )
};
