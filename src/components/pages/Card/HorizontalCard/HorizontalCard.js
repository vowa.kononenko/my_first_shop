import React from "react";
import { Link } from "react-router-dom";

import style from './HorizontalCard.module.scss'

export default function HorizontalCard() {
    return (
        <div className={style.horizontalCard}>
            <div className={style.horizontalCard__firstBlock}>
                <img className={style.horizontalCard__img}  src="https://brain.com.ua/static/images/prod_img/0/1/U0499901_big.jpg" alt=""/>
                <Link className={style.horizontalCard__title}>Ноутбук ASUS X515JP-BQ031 (90NB0SS1-M00620)</Link>
            </div>

            <div className={style.horizontalCard__secondBlock}>
                <div className={style.horizontalCard__countBlock}>
                    {/* <div className=""> */}
                        <div className={style.horizontalCard__countBtn}>-</div>
                    {/* </div> */}
                    <span className={style.horizontalCard__count}>2</span>
                    {/* <div className=""> */}
                        <div className={style.horizontalCard__countBtn}>+</div>
                    {/* </div> */}

                </div>
                <span className={style.horizontalCard__price}>70 599 ₴</span>
            </div>
        </div>
    )
};
