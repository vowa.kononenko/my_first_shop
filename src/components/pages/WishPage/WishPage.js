import React from 'react';
import { Col, Row } from 'reactstrap';
import ProductCard from '../../ProductCard/ProductCard';

import style from './WishPage.module.scss'


export default function WishPage(){
    return (

        <Row>
            <Col md={{size:10, offset:1}} >
            <div className={style.wishPage__title}>
                <h3 className='mt-2'>Желания</h3>
            </div>

            <div>
                <ul className={style.cardGroup}>
                    <li>
                    <ProductCard title='Ноутбук ASUS X515JP-BQ031 (90NB0SS1-M00620)' />
                    </li>
                    <li>
                    <ProductCard title='Ноутбук ' />
                    </li>
                    <li>
                    <ProductCard title='Ноутбук ASUS X515JP-BQ031 (90NB0SS1-M00620)' />
                    </li>                    <li>
                    <ProductCard title='Ноутбук ASUS X515JP-BQ031 (90NB0SS1-M00620)' />
                    </li>  
                    <li>
                    <ProductCard title='Ноутбук ASUS X515JP-BQ031 (90NB0SS1-M00620)' />
                    </li>                  
                    <li>
                    <ProductCard title='Ноутбук ASUS X515JP-BQ031 (90NB0SS1-M00620)' />
                    </li>
                    <li>
                    <ProductCard title='Ноутбук ASUS X515JP-BQ031 (90NB0SS1-M00620)' />
                    </li>

                </ul>
            </div>
            </Col>

        </Row>

    )
}