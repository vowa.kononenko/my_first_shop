import React from "react";
import { Col, Container, Row } from "reactstrap";
import CategoryHeader from "./CategoryHeader/CategoryHeader";
import Filter from "./Filter/Filter";
import ProductList from "./ProductList/ProductList";

export default function CategoryPage() {
    return (
        <Container fluid={true}>
            <Row>
                <CategoryHeader/>
            </Row>
            <Row>
                <Col md={{ size: 3 }}>
                    <Filter />
                </Col>
                <Col md={{ size: 9 }} xs={{ size: 12 }}>
                    <Row>
                        <Col
                            md={{ size: 12, offset: 0 }}
                            xs={{ size: 12, offset: 0 }}
                        >
                            <ProductList />
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Container>
    );
}
