import React from "react";

import style from "./CategoryHeader.module.scss";

export default function CategoryHeader() {
    return (
        <div className={style.categoryHeader}>
            <h3>Название категории</h3>
            <select  className={style.categoryHeader__sort + " form-select "} aria-label="Default select example">
                <option selected>Сортировать</option>
                <option value="1">Цена по возврастанию</option>
                <option value="2">цена по убыванию</option>
                <option value="3">А =&gt; Я</option>
                <option value="4">Я =&gt; А</option>


            </select>
        </div>
    );
}
