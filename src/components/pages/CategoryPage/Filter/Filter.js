import React from "react";
import { Accordion, AccordionBody, AccordionHeader, AccordionItem } from "../../../Accordion/Accordion";

import style from "./Filter.module.scss";

export default function Filter() {
    return (
        <div className={style.filter}>
            <Accordion>
                <AccordionItem>
                    <AccordionHeader>
                            Фильтр #1
                    </AccordionHeader>
                    <AccordionBody>

                            <ul className="list-unstyled">
                                <li>
                                    <input
                                        type="checkbox"
                                        name=""
                                        id="happy"
                                        className={style.filter__checkbox}
                                    />
                                    <label for="happy">Значаение #1</label>
                                </li>
                                <li>
                                    <input
                                        type="checkbox"
                                        name=""
                                        id="happy"
                                        className={style.filter__checkbox}
                                    />
                                    <label for="happy">Значаение #2</label>
                                </li>
                                <li>
                                    <input
                                        type="checkbox"
                                        name=""
                                        id="happy"
                                        className={style.filter__checkbox}
                                    />
                                    <label for="happy">Значаение #3</label>
                                </li>
                                <li>
                                    <input
                                        type="checkbox"
                                        name=""
                                        id="happy"
                                        className={style.filter__checkbox}
                                    />
                                    <label for="happy">Значаение #4</label>
                                </li>
                                <li>
                                    <input
                                        type="checkbox"
                                        name=""
                                        id="happy"
                                        className={style.filter__checkbox}
                                    />
                                    <label for="happy">Значаение #5</label>
                                </li>
                            </ul>
                    </AccordionBody>
                </AccordionItem>
            </Accordion>
                

            <Accordion>
                <AccordionItem>
                    <AccordionHeader>
                            Фильтр #1
                    </AccordionHeader>
                    <AccordionBody>

                            <ul className="list-unstyled">
                                <li>
                                    <input
                                        type="checkbox"
                                        name=""
                                        id="happy"
                                        className={style.filter__checkbox}
                                    />
                                    <label for="happy">Значаение #1</label>
                                </li>
                                <li>
                                    <input
                                        type="checkbox"
                                        name=""
                                        id="happy"
                                        className={style.filter__checkbox}
                                    />
                                    <label for="happy">Значаение #2</label>
                                </li>
                                <li>
                                    <input
                                        type="checkbox"
                                        name=""
                                        id="happy"
                                        className={style.filter__checkbox}
                                    />
                                    <label for="happy">Значаение #3</label>
                                </li>
                                <li>
                                    <input
                                        type="checkbox"
                                        name=""
                                        id="happy"
                                        className={style.filter__checkbox}
                                    />
                                    <label for="happy">Значаение #4</label>
                                </li>
                                <li>
                                    <input
                                        type="checkbox"
                                        name=""
                                        id="happy"
                                        className={style.filter__checkbox}
                                    />
                                    <label for="happy">Значаение #5</label>
                                </li>
                            </ul>
                    </AccordionBody>
                </AccordionItem>
            </Accordion>
            <Accordion>
                <AccordionItem>
                    <AccordionHeader>
                            Фильтр #1
                    </AccordionHeader>
                    <AccordionBody>

                            <ul className="list-unstyled">
                                <li>
                                    <input
                                        type="checkbox"
                                        name=""
                                        id="happy"
                                        className={style.filter__checkbox}
                                    />
                                    <label for="happy">Значаение #1</label>
                                </li>
                                <li>
                                    <input
                                        type="checkbox"
                                        name=""
                                        id="happy"
                                        className={style.filter__checkbox}
                                    />
                                    <label for="happy">Значаение #2</label>
                                </li>
                                <li>
                                    <input
                                        type="checkbox"
                                        name=""
                                        id="happy"
                                        className={style.filter__checkbox}
                                    />
                                    <label for="happy">Значаение #3</label>
                                </li>
                                <li>
                                    <input
                                        type="checkbox"
                                        name=""
                                        id="happy"
                                        className={style.filter__checkbox}
                                    />
                                    <label for="happy">Значаение #4</label>
                                </li>
                                <li>
                                    <input
                                        type="checkbox"
                                        name=""
                                        id="happy"
                                        className={style.filter__checkbox}
                                    />
                                    <label for="happy">Значаение #5</label>
                                </li>
                            </ul>
                    </AccordionBody>
                </AccordionItem>
            </Accordion>
        </div>
    );
}
