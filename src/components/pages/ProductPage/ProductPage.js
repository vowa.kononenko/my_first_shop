import React from 'react';
import {Container, Row, Col} from 'reactstrap';

import PopularProductsList from '../../PopularProductsList/PopularProductsList';
import Characteristics from './Characteristics/Characteristics';
import Description from './Description/Description';
import MainRight from './MainRight/MainRight';
import PicBlock from './PicBlock/PicBlock';

export default function ProductPage() {
    return (
        <Container fluid= {true}>
            <Row>   
                <Col  md={{size: 6}} className='ProductPage__aboutLeft'>
                    <PicBlock/>
                </Col>
                <Col md={{size: 6}} xs={{size: 12}}>
                        
                            <MainRight/>


                </Col>
            </Row>
            <Row>   
                <Col  md={{size: 6}} className='ProductPage__aboutLeft'>
                    <Characteristics/>
                </Col>
                <Col md={{size: 6}} xs={{size: 12}}>
                        
                            <Description/>


                </Col>
            </Row>
            <Row>
                        <Col>
                            <PopularProductsList/>
                        </Col>
                    </Row>
                    
    </Container>
)   
            
}