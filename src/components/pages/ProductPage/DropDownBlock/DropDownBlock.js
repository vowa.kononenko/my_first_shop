import React, { useRef, useState } from 'react';

import style from './DropDownBlock.module.scss'

export default function DropDownBlock({children}) {
    const [open, setOpen] = useState(false)
    let maxHeight,toggleText,opacityBtn;

    const content = useRef()

    function onToggleButton(){
        setOpen( (open)=>!open)
    }


    if(open){
        toggleText='Скрыть';
        opacityBtn='0.1';
        maxHeight = `${content.current.scrollHeight}px`
    } else {
        toggleText='Показать содержимое';
        opacityBtn='0.5';
        maxHeight = `200px`
    }
    
    return (
        <div className ={style.dropDownBlock} >
            <div style={{maxHeight:maxHeight}} ref={content} className={`${style.dropDownBlock__content} `}>
            {children}

            </div>
            <div onClick={onToggleButton}   className={style.dropDownBlock__btn} >
                    <span>{toggleText}</span>
                    <div style={{opacity:opacityBtn}} className={style.dropDownBlock__btn__bottom}></div>
            </div>
        </div>
        
    )
};
