import React from 'react';
import { ListGroup, ListGroupItem } from 'reactstrap';
import DropDownBlock from '../DropDownBlock/DropDownBlock';

import style from './Characteristics.module.scss'

export default function Characteristics() {
    return (
        <DropDownBlock>
            <h2 className={style.characteristics__title}>Характеристики</h2>
            <dl className={style.characteristics__list}>    
                <div className={style.characteristics__item}>
                    <dt className={style.characteristics__label}>
                        <span>Характеристика</span> 
                    </dt>
                    <dd className={style.characteristics__value}>
                    <span>Значение</span> 
                    </dd>
                </div>
                <div className={style.characteristics__item}>
                    <dt className={style.characteristics__label}>
                        <span>Характеристика</span> 
                    </dt>
                    <dd className={style.characteristics__value}>
                    <span>Значение</span> 
                    </dd>
                </div>
                <div className={style.characteristics__item}>
                    <dt className={style.characteristics__label}>
                        <span>Характеристика</span> 
                    </dt>
                    <dd className={style.characteristics__value}>
                    <span>Значение</span> 
                    </dd>
                </div>
                <div className={style.characteristics__item}>
                    <dt className={style.characteristics__label}>
                        <span>Характеристика</span> 
                    </dt>
                    <dd className={style.characteristics__value}>
                    <span>Значение</span> 
                    </dd>
                </div>
                <div className={style.characteristics__item}>
                    <dt className={style.characteristics__label}>
                        <span>Характеристика</span> 
                    </dt>
                    <dd className={style.characteristics__value}>
                    <span>Значение</span> 
                    </dd>
                </div>
            </dl>

        </DropDownBlock>
        
    )
};
