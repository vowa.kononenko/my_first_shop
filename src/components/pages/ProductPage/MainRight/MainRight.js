import React from 'react';
import Button from '../../../Button/Button';

import style from './MainRight.module.scss'

import srcShopcart from '../../../../assets/svg/header/shopсart.svg';
import srcHeart from '../../../../assets/svg/header/heart.svg';

export default function MainRight() {
    return (
        <div className={style.aboutRight}>
            <div className={style.aboutRight__topBlock} >
                <h3 className={style.aboutRight__title} >Монітор 27" Samsung Odyssey G5 LC27G54T Black (LC27G54TQWIXCI) + DisplayPort и HDMI кабель</h3>
                <div className={style.aboutRight__codeBlock}>
                    <h4 className={style.aboutRight__codeTitle}>Код товара: </h4>
                    <span className={style.aboutRight__codeValue}>09970887</span>   
                </div>
                <h1 className={style.aboutRight__price}>
                    72 599 ₴
                </h1>
            </div>
            <div className={style.aboutRight__bottomBlock} >
                <div className={style.aboutRight__bottomWrap}>
                <Button>                                    
                    <svg >
                        <use href={srcShopcart +'#Layer_1'}></use>
                    </svg>
                    <h3>Корзина</h3>
                </Button>
                <Button color='secondary'>
                    <svg >
                        <use href={srcHeart +'#heart'}></use>
                    </svg>
                    <h3>Желания</h3>
                </Button>
                </div>


            </div>

        </div>
    )
};
