import React from 'react';
import DropDownBlock from '../DropDownBlock/DropDownBlock';

import style from './Description.module.scss'

export default function Description() {
    return(
        <DropDownBlock className={style.description}>
            <h2 className={style.description__title}>Описание</h2>
            <div className={style.description__content}>
                <span>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Labore architecto similique alias consequatur, doloribus modi magni minima iure illum itaque quam hic fugit tempora deleniti qui illo maiores nisi veritatis?
                Dolor reiciendis labore iusto dignissimos, deleniti doloremque odio? Esse velit quidem aspernatur similique repellendus ad eveniet officiis doloremque numquam voluptate, libero qui optio, voluptas quod molestiae sapiente. Distinctio, aperiam enim?
                Ratione, dolorem quidem iure eaque, architecto amet ullam, vel illo in ipsum esse repellat aspernatur quae aliquam blanditiis aperiam adipisci perspiciatis id. Suscipit officiis eveniet ipsum laboriosam eligendi tempore fuga.
                Veniam dolores iusto minus tenetur molestiae labore vitae architecto error, provident ullam totam quibusdam tempora modi accusantium deleniti! Ab, nemo aut animi reiciendis mollitia atque! Nostrum est doloremque in inventore.
                </span>
 
            </div>

        </DropDownBlock>
    )
};
