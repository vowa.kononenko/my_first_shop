import React from 'react';

import style from './PicBlock.module.scss'

export default function PicBlock() {
    return(
        <div className={style.picBlock}>
            <div className={style.picBlock__thumbnailsList__wrap}>
                <ul className={style.picBlock__thumbnailsList}>
                    <li>
                        <img src="https://brain.com.ua/static/images/prod_img/0/1/U0499901_big.jpg" alt=""/>
                    </li>
                    <li>
                        <img src="https://brain.com.ua/static/images/prod_img/0/1/U0499901_2big.jpg" alt=""/>
                    </li>
                    <li>
                        <img src="https://brain.com.ua/static/images/prod_img/0/1/U0499901_3big.jpg" alt=""/>
                    </li>
                    <li>
                        <img src="https://brain.com.ua/static/images/prod_img/0/1/U0499901_4big.jpg" alt=""/>
                    </li>
                    <li>
                        <img src="https://brain.com.ua/static/images/prod_img/0/1/U0499901_5big.jpg" alt=""/>
                    </li>
                    <li>
                        <img src="https://brain.com.ua/static/images/prod_img/0/1/U0499901_6big.jpg" alt=""/>
                    </li>
                    <li>
                        <img src="https://brain.com.ua/static/images/prod_img/0/1/U0499901_7big.jpg" alt=""/>
                    </li>
                    <li>
                        <img src="https://brain.com.ua/static/images/prod_img/0/1/U0499901_8big.jpg" alt=""/>
                    </li>
                    <li>
                        <img src="https://brain.com.ua/static/images/prod_img/0/1/U0499901_9big.jpg" alt=""/>
                    </li>
                    <li>
                        <img src="https://brain.com.ua/static/images/prod_img/0/1/U0499901_10big.jpg" alt=""/>
                    </li>
                </ul>
            </div>
            
            <img className={style.picBlock__bigImg} src="https://brain.com.ua/static/images/prod_img/0/1/U0499901_big.jpg" alt=""/>
        </div>
    )
};
