import React from 'react';

import srcHeart from '../../../assets/svg/header/heart.svg';
import srcShopcart from '../../../assets/svg/header/shopсart.svg';
import srcPhone from '../../../assets/svg/header/phone.svg';
import srcCallback from '../../../assets/svg/header/callback.svg';
import srcBurger from '../../../assets/svg/header/burger.svg';


import style from '../Header.module.scss'
import { Link } from 'react-router-dom';

export default function ViewMain() {
    return (
        <header className={style.header}>
            <div className={style.header__info}>
                <div className={style.header__tell}>
                    <svg >
                            <use href={srcPhone +'#Capa_1'}></use>
                    </svg>
                    <span>+380962938604</span>
                </div>
                <div className={style.header__callback}>
                    <svg >
                            <use href={srcCallback +'#Capa_2'}></use>
                    </svg>
                    <span>Вам перезвонить?</span>
                </div>
            </div>

            <div className={style.header__main}>
                <Link to='/' className={style.header__name}>
                    <h1>Test Shop</h1>
                </Link>
                <div className={style.header__burger}>
                    <svg  >
                                <use href={srcBurger +'#burger'}></use>
                    </svg>  
                </div>
                
                <div className={style.header__search}>
                    <input  type="text" placeholder='Поиск'/>
                    <button  type='button'>Найти</button>
                </div>
                <ul className={style.header__actions}>
                    <li className= {style.header__heart}>
                        <Link to='/wish' className= {style.action__group}>
                            <span className={style.action__caunter}>2</span>
                            <svg >
                                <use href={srcHeart +'#heart'}></use>
                            </svg>
                            <span>Желания</span>
                        </Link>
                        
                        
                    </li>
                    <li>
                        <Link to='/cart' className= {style.action__group}>
                            <span className={style.action__caunter}>3</span>
                            <svg >
                                <use href={srcShopcart +'#Layer_1'}></use>
                            </svg>
                            <span>Корзина</span>
                        </Link>
                        

                    </li>
                </ul>
            </div>
            
            
        </header>
    )
    
}