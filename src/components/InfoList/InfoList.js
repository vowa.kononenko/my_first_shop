import React from 'react';
import { GroupList, GroupListItem } from '../ListItems/ListItems';



export default function InfoList({...props}) {

    return (
        <GroupList {...props} >
            <GroupListItem to='/info/about'>O нас</GroupListItem>
            <GroupListItem to='/info/partners'>Партнеры</GroupListItem>
            <GroupListItem to='/info/dealer'>Оптовым покупателям</GroupListItem>
            <GroupListItem to='/info/vacancies'>Вакансии</GroupListItem>
            <GroupListItem to='/info/delivery'>Доставка и оплата</GroupListItem>
            <GroupListItem to='/info/returns'>Обмен и возврат</GroupListItem>
            <GroupListItem to='/info/warranty'>Сервис и гарантия</GroupListItem>
            <GroupListItem to='/info/terms'>Условия использования сайта</GroupListItem>
        </GroupList>
    )
}