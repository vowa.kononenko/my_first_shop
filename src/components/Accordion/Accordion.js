import React from "react";
import { v4 as uuidv4 } from 'uuid';


import style from "./Accordion.module.scss";


export  function Accordion({children,...props}) {
    return(
        <div
        {...props}
        className="accordion accordion-flush"
        // id="accordionFlushExample"
        >
            {children}
        </div>  
    )
}

export  function AccordionItem({children,open=false,...props}){
    let uuid = uuidv4();
    return(
        <div {...props} className="accordion-item">
            {React.Children.map(children, (ThisArg)=>React.cloneElement(ThisArg,{uuid,open}) )}
        </div>
    )

}
export  function AccordionHeader({children,uuid,open,...props}){
    return (
        <h2 {...props} className="accordion-header" id={`flush-heading${uuid}`}>
            <button
                className={
                    `${style.accordion__btn} accordion-button ${open||'collapsed'}`
                }
                type="button"
                data-bs-toggle="collapse"
                data-bs-target={`#flush-collapse${uuid}`}
                aria-expanded={open}
                aria-controls={`flush-collapse${uuid}`}
            >
                {children}
            </button>
        </h2>
    )
}
export  function AccordionBody({children,uuid,open,...props}){
    return(
        <div
            id={`flush-collapse${uuid}`}
            className={`accordion-collapse collapse ${open&&'show'} ${style.accordion__body}`}
            aria-labelledby={`flush-heading${uuid}`}
            // data-bs-parent="#accordionFlushExample"
        >
            <div className={`accordion-body ${style.accordion__body2}`}>
                {children}                            
            </div>
        </div>
    )
}