import React from 'react';

import style from './SideMenu.module.scss'

import srcPhone from '../../assets/svg/header/phone.svg';
import { Link } from 'react-router-dom';
import { GroupList, GroupListItem } from '../ListItems/ListItems';
import { Accordion, AccordionBody, AccordionHeader, AccordionItem } from '../Accordion/Accordion';
import CategoryList from '../CategoryList/CategoryList'
import InfoList from '../InfoList/InfoList';

import srcShopcart from '../../assets/svg/header/shopсart.svg';
import srcHeart from '../../assets/svg/header/heart.svg';
import srcCategory from '../../assets/svg/category.svg';
import srcInfo from '../../assets/svg/info.svg';




export default function SideMenu() {
    // document.body.style.position = 'fixed'
    // document.body.style.width = '100%'
    return (
        <div className={style.sideMenu__overley}>
            <nav className={style.sideMenu__drawer}>
                <div className={style.sideMenu__header}>
                    <div className={style.sideMenu__firstBlock}>
                        <Link to='/' className={style.sideMenu__logo}>
                            <h1>Test Shop</h1>
                        </Link>
                        <span className={style.sideMenu__close}>&#10006;</span>
                    </div>
                    <a className={style.sideMenu__phoneNumber} href='tel:0962938604' content="+380962938604">
                            <svg >
                                    <use href={srcPhone +'#Capa_1'}></use>
                            </svg>
                            <b>(096) 293-86-04</b>
                    </a>                    
                </div>
                <div className={style.sideMenu__body}>
                        
                        <GroupList >
                            <GroupListItem effect={false} >
                                <Accordion>
                                    <AccordionItem open>
                                        <AccordionHeader>
                                            <div className={style.sideMenu__actionGroup}>
                                                <svg >
                                                    <use href={srcCategory +'#category'}></use>
                                                </svg>
                                                <h3>Категории</h3>
                                            </div>

                                        </AccordionHeader>
                                        <AccordionBody>
                                        <CategoryList/>

                                        </AccordionBody>
                                    </AccordionItem>
                                </Accordion>
                            </GroupListItem>
                            <GroupListItem >
                                <Link to='/cart' className={`${style.sideMenu__actionGroup} ${style.sideMenu__sideBtn} `}>
                                    <svg >
                                        <use href={srcShopcart +'#Layer_1'}></use>
                                    </svg>
                                    <h3>Корзина</h3>
                                </Link>
                                
                            </GroupListItem>
                            <GroupListItem >
                                <Link to='/wish' className={`${style.sideMenu__actionGroup} ${style.sideMenu__sideBtn} `}>
                                    <svg >
                                        <use href={srcHeart +'#heart'}></use>
                                    </svg>
                                    <h3>Желания</h3>
                                </Link>

                                
                            </GroupListItem>
                            <GroupListItem effect={false} >
                                <Accordion>
                                    <AccordionItem>
                                        <AccordionHeader>
                                            <div className={style.sideMenu__actionGroup}>
                                                <svg >
                                                    <use href={srcInfo +'#info'}></use>
                                                </svg>
                                                <h3>Инфо</h3>
                                            </div>
                                        </AccordionHeader>
                                        <AccordionBody>
                                            <InfoList/>
                                        </AccordionBody>
                                    </AccordionItem>
                                </Accordion>
                                
                            </GroupListItem>

                        </GroupList>
                </div>
            </nav>
        </div>
    )
}