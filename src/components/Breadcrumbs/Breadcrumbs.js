import React from 'react';
import { Link } from 'react-router-dom';

import style from './Breadcrumbs.module.scss'

export default function Breadcrumbs() {
    return (
        <div className={style.breadcrumbs}>
            <Link className={style.link}>
                <h6>{"Home "}</h6>  
            </Link>
            <h6 className={style.slash}>/</h6>
            <Link className={style.link} >
                <h6> Computers</h6>
            </Link>
            
        </div>
    )
}